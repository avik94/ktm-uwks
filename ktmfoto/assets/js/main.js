function numberWithCommas(x) {
    return x.toLocaleString('en-US', {minimumFractionDigits: 0});
}

function generateAlertWaring(message) {
	alert_warning = '';
	alert_warning += '<div class="alert alert-dismissible alert-warning">';
	alert_warning += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
	alert_warning += '<p><strong>Peringatan!</strong> '+message+'</p>';
	alert_warning += '<div>'

	return alert_warning;
}

function generateAlertDanger(message) {
	alert_warning = '';
	alert_warning += '<div class="alert alert-dismissible alert-danger">';
	alert_warning += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
	alert_warning += '<p><strong>Gagal!</strong> '+message+'</p>';
	alert_warning += '<div>'

	return alert_warning;
}

function generateAlertSuccess(message) {
	alert_warning = '';
	alert_warning += '<div class="alert alert-dismissible alert-success">';
	alert_warning += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
	alert_warning += '<p><strong>Sukses!</strong> '+message+'</p>';
	alert_warning += '<div>'

	return alert_warning;
}

function getBase64Photo(photo) {
	return 'data:image/png;base64,' + photo;
}

function generateDefaultPhoto() {
	return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADwCAMAAABheJuYAAAAYFBMVEWAgYP///99foB5enz7+/v19fX4+PiLjI6DhIbx8fGIiYv09PR3eHqOj5GlpqevsLGTlJbd3d7l5eXKysvExMXR0dKfoKHr6+y9vb7Y2dmampyqq6y2t7jn5+i/v8Cjo6Vb0X76AAAIvUlEQVR4nO2d67aqOgyFWUEFFBRQRMHL+7/lBhEpFxWSiegenWOcP2vsI/2kbdIkjcbfj8uYegBSaYCppQGmlgaYWhpgammAqaUBppYGmFpjAywyzbP/ZmM9YEQAJzxuL5vrep2s19fNJQrOY1CMBXDebfaeYdJDpmn76SWAP2gUgPiUWAaR0VCGsfS3C+yz4ACLeGe57cE/IEwzjZFTCQywClLbfDr68kWczrgnQgHm26v9ZvQFgneBTSQkwNbrNfycwPAd0ENhAIvo7dypIZgHzEtAAZz3Zv/RFwhpjHgwCCDyBnz7JYEfAp6MAdj1nfx1Ag9g1yAAG5cx/JyAtuJnIwAi5vhziQkAAMclf/wGRZMDxAln/j8AbCGBGGC2Gbp/NgiE70AMcJR8/zeCpWgvEgMwDECTwJLYZClAJJtAN5npdAAL+QvIRJfJACIbMH6RSZYBLFLEC8gdO/YhTQYQSmxYjeA4DcABsIQLgOUkACsfM4MyudxXIAIIYeM3aM9cBSKADQ7AWDJfgQgAsofeRZvPAziCc0AbIOEdkSUAW9QeVIg3hyQAICt2l3n4OABuE81FyfzDAGcPOf5MrGCdAADjyFVyWR6dAGCHHb9h7j4McIIuAa4l4APM12iA5LMAjiic0gXgfxYghJwma/osQIA6zDzkfhZAHBCaGgARUNEAoimEHv+nAUILPH7yPgsQY53RDGD9WQC4JTZZ2RqBL3RAA7DOlN/jTpO3+jBAjPUl6MSKDEmOlHvsmZiXavqaqAT5vBocCcAKGhdiRthFkTmkJbCZKW8RgCRF3xB5zAIiEcActw/RiTkGWYLjgssPcMvoZACwUxnPkfsiAKYRkAI4sG1oyS7EFAEcYbsQedw8qwhghzPFJjdZ/y05MvKZFR9fAzCJIQOGd6cBALqj0wAAQ0O/D8CKrUsBYqA3OgmAA3wD3LIzGQAuOMetNPiachu2NycCWOCCc+Yk9UIznCVzWVEtKQDQkrFC63IA3ImMlaGUA6yuoDnE3oSkha+ocoOJjpS4wAqxbzRJi7+PkGVMe3YBu7j8fo0gEFwGEgMgIhPk8+/Eye/QHJfiOxy24A4H4BaTONVEkht9iHtkQo+IWa4IBAhE5xras6oVkQB/luQV2OyrAzgAiU9HogkEApjzt1KSXu0GXcdlA/DqC+AA/Po5kj4adKObm2sSvwAUQMyLT5Av2kJzoZoC8ALVX3KjOxfrNofEiSsF6yvBWQXs5LAiGIDDON9Lb3PnggFwYkSyq9CFcL1VGLH27wJgxIgsQGuPKQHYWRlVkwL43ICoIiDA4JMlOyujaspF/F0As+F1sOy0kipch6fhcV5+RFcRDGA1PLgi96X/gAAMO8YPSSuCATB6xLjf06LqLzcDg9eA+VUAf9FwAG4jAFUggPlscLaGDP8ijAnlAgHc8hOnIcuArMPiD9C+EwPg3LyyxYB6fDfNF8DqIHbnMABBMY7eVyLI3BXTPxRPIgjAubRI/W42kZE+vvjj9F3OMu0fowh6vAPTUwf9DbHRrVs5NYHxrl2qkYb1/1fW9RIAkJkw5Wx4fE1AdqRu/nNf2q5QDhBm817NEr2aRWQ3butd8r9O26YtvN1Mt5SZfE6e2QNKGkv2tujJm7RNW/F910qf42snAbmX5oItwnmmJEAnA3A2ZaNRW/3z/NTRvdZstzKblw6gYBkIAObhvhpno6tF0Gx5QFZHeXrpe5Ag0ccFcI4HX/2aKal7louN6l4TJR2us/P4F+SxPWsWQLjbJEZjlrTChMfqJdBy12WulJwCP9c3GGAebbobq7e8mlXqFv/KTTovmNSuhHe+oj4aAjBzgpPlmt2N1btaTB1vQ7SeNH2pJ3XItU4xYz/tDTA7R5ul+8LKdi3EODVo/yQLEzTTamTSfnccOpV6Aqyi1H/e0v6mrpoNJ306NzrzCWQa+1M0iKEXwOrg22+G35lxdPz878vOAcVP4khEtpdG/Q/L7wHmx+uL3xNQ1O42WOyk3d0KXtTo5D8X4W/jfhnYNwCL4PJu6lTPbQ60bCXpdqyCd1nN24IIekymlwDn7Xo5oKV9o2zgkTDoioH2yCuTaSfvF8QLgOPaenc6qave5UvpO9FOJfVsC5IviPXrBfEEYB5uaMjPCRSP26sfoVzza7+C/on9bEG4yfa5hegCWAS7ZPDoc3nKZA/VXcZq+BktG/AGwjSu26A7H9UGCHfpkIlfk+IW12JEjZ4RjJwymcvroevo1gSI9gMnfu0p1VRplCTbtY3omQ148+m0TE6tDU0FmAfr4RO/psdUmTfqAOtbLLtOk8hdHs619WBUo78kr1ydfg8o51Ar0KvagoXoMdl6iM4tgDjb8fsarBeidfHtOK0oqboRSXslE/mbqAYQpB5/4tdVbBUdSe/KFgDKxTP7UAY4jMzRfPXbSQN1DzDE7TdQZeUxtz4yO73NvSUj8GXrtvGx1pNBVhlJ2L0bw/W35z9DVDbc8aHFYl20psljBRxxfZWyxXCCfdhdZYSrURBeFZcBL8/lnwtvN0j+3ebWV0F1yWcFfuVwLe+7Q1yLC1Ul9iGyKdEYekwWtZ5aCZ2Cm1XjRfv7EUT9fRrlp1rQjW5HUHk03lZ/Utzpb18CiutcXZdWG4B9+xLIbVlpcx+tzZWoHebm37gygxZAVSP9A0ugOhon7TcwQ7fIHEXurDHYag3gOw2PoXslllJ48PCl0e3OxxFdm9/242dmkL/aMaKs24yJKoDSmR6h2fY42jUMWWkbhld1TaPiaKxUwt6dOawrPaKKpKM63wvjtvqVGXSb8ys1+lNUWOLauo2t/JJ/7dcSizw4uk/yiKJzfccprPPvjD8/wdSNrvknvTj9YdmNYup8VfzQDMr7h9bPjtkccn5mD8pFaf1CBHkO+ndrRpbVjC7ufsWKlWoOd/kTnrSWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpbW/6t/q36TV13B5EoAAAAASUVORK5CYII=";
}