<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Qr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->library('ciqrcode');

        $params['data'] = 'This is a text to encode become QR Code';
        $params['level'] = 'H';
        $params['size'] = 5;
        $params['savename'] = FCPATH . 'tes.png';
        $this->ciqrcode->generate($params);

        $arrContextOptions = [
            'ssl' => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ];

        $imagedata = file_get_contents(base_url() . 'tes.png', false, stream_context_create($arrContextOptions));
        // alternatively specify an URL, if PHP settings allow
        $base64 = base64_encode($imagedata);

        echo '<img src="data:image/png;base64,' . $base64 . '"/>';
    }

    public function otg()
    {
        $this->load->library('ciqrcode');

        // header("Content-Type: image/png");
        header("Content-Type: image/png;base64");
        $params['data'] = 'This is a text to encode become QR Code';
        $hello = $this->ciqrcode->generate($params);
        var_dump($hello);

    }
}
