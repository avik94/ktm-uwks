<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rec extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [
            'title' => 'Rekam KTM Mahasiswa',
            'content' => 'rec/index',
        ];

        $this->load->view('template', $data);
    }

    public function generateQr()
    {
        $this->load->library('ciqrcode');

        $params['data'] = trim($_POST['npm']);
        $params['level'] = 'H';
        $params['size'] = 5;
        $params['savename'] = FCPATH . 'tes.png';
        $this->ciqrcode->generate($params);

        $arrContextOptions = [
            'ssl' => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ];

        $imagedata = file_get_contents(base_url() . 'tes.png', false, stream_context_create($arrContextOptions));
        // alternatively specify an URL, if PHP settings allow
        $base64 = base64_encode($imagedata);

        echo json_encode(['base64_qrcode' => $base64]);
    }

}

/* End of file Rec.php */
/* Location: ./application/controllers/Rec.php */
