<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_coba extends CI_Model {


	public function __construct(){
		parent::__construct();
	}

	public function add($blobdata) {
		return $this->db->insert('tb_coba',$blobdata);
	}

	public function show($id) {
		$query = $this->db->get_where('tb_coba', array('id' => $id));
		return $query->row();
	}

}

/* End of file M_coba.php */
/* Location: ./application/models/M_coba.php */