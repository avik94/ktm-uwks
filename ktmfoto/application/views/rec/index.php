<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>KTM UWKS | <?=$title?></title>
	<link rel="shorcut icon" href="<?=base_url('assets/img/logo-uwks.png')?>">

	<!-- css lib -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/bootstrap-flatly.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/font-awesome-4.7.0.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/main_styles.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/content_styles.css')?>">

	<!-- js lib -->
	<script type="text/javascript" src="<?=base_url('assets/js/jquery-3.1.1.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/js/main.js')?>"></script>

	<!-- Cropper -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/cropper.min.css')?>">
	<script type="text/javascript" src="<?=base_url('assets/js/cropper.min.js')?>"></script>

	<!-- photobooth -->
	<script type="text/javascript" src="<?=base_url('assets/js/photobooth_min.js')?>"></script>

</head>
<body>

	<div class="container-fluid" style="padding-top: 0px;">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">KTM Mahasiswa UWKS</h3>
					</div>
					<div class="panel-body">
						<div class="container-fluid">
							<div class="row">
								<div class="col-sm-5 col-md-5 col-lg-5 col-form">
									<form class="form-horizontal">
										<div class="well">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label for="npm" class="col-md-4 control-label">NPM</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="npm" id="npm" placeholder="NPM" value="">
														</div>
													</div>
													<div class="form-group">
														<label for="nama" class="col-md-4 control-label">Nama</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="" readonly>
														</div>
													</div>
													<div class="form-group">
														<label for="prodi" class="col-md-4 control-label">Prodi</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="prodi" id="prodi" placeholder="Prodi" value="" readonly>
														</div>
													</div>
													<div class="form-group">
														<label for="alamat" class="col-md-4 control-label">Alamat</label>
														<div class="col-md-8">
															<textarea class="form-control" rows="3" name="alamat" id="alamat" readonly>Alamat</textarea>
														</div>
													</div>
													<div class="form-group">
														<label for="foto" class="col-md-4 control-label">Foto</label>
														<div class="col-md-8" style="padding: auto;">
															<img src="" alt="Foto Mahasiswa" class="img-responsive img-thumbnail foto" id="foto">
														</div>
													</div>
													<div class="form-group">
														<label for="tgl_cetak" class="col-md-4 control-label">Tgl Cetak KTM</label>
															<div class="col-md-8">
																<input type="text" class="form-control" name="tgl_cetak" id="tgl_cetak" placeholder="Tanggal Cetak" value="" readonly>
															</div>
														</div>
													<div class="form-group">
														<label for="status_ktm" class="col-md-4 control-label">Status KTM</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="status_ktm" id="status_ktm" placeholder="Status KTM" value="" readonly>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<button type="submit" id="btnCari" name="btnCari" class="btn btn-primary btn-block">
														<i class="fa fa-search" aria-hidden="true"></i> Cari
													</button>
												</div>
												<div class="col-md-4">
													<button type="button" id="btnCetak" name="btnCetak" class="btn btn-primary btn-block">
														<i class="fa fa-print" aria-hidden="true"></i> Cetak
													</button>
												</div>
												<div class="col-md-4">
													<button type="button" id="btnTulis" name="btnTulis" class="btn btn-primary btn-block">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Tulis NFC
													</button>
												</div>
											</div>
										</div> <!-- well -->
									</form>
								</div> <!-- col-form -->
								<div class="col-sm-7 col-md-7 col-lg-7 col-photo-preview">
									<div class="well">
										<div class="row" style="margin-bottom: 20px;">
											<div class="col-md-12 live-view" id="live-view">
												<div id="live-view-content" class="live-view-content"></div>
											</div>
											<div class="col-md-12" id="capture-view" style="display: none;">
												<img src="" class="img-responsive img-capture-view" id="img-capture-view">
											</div>
											<div class="col-md-12" id="crop-view" style="display: none;">

											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<button id="btnLive" class="btn btn-primary btn-block">
													<i class="fa fa-video-camera" aria-hidden="true"></i> <strong>Live</strong>
												</button>
											</div>
											<div class="col-md-4">
												<button id="btnCapture" class="btn btn-primary btn-block">
													<i class="fa fa-camera" aria-hidden="true"></i> <strong>Capture</strong>
												</button>
											</div>
											<div class="col-md-4">
												<button id="btnCrop" class="btn btn-primary btn-block">
													<i class="fa fa-crop" aria-hidden="true"></i> <strong>Crop</strong>
												</button>
											</div>
										</div>
									</div>
								</div> <!-- col-photo-preview -->
							</div>
						</div> <!-- container-fluid -->
					</div> <!-- panel body -->
				</div>
			</div>
		</div>
	</div>

<!-- <button id="btnCoba" class="btn btn-primary btn-block">
	<i class="fa fa-crop" aria-hidden="true"></i> <strong>Coba</strong>
</button> -->

	<script type="text/javascript">
		var $image_captured, datas;

		$(function(){
			$image_captured = $('#img-capture-view');

			$('#live-view-content').photobooth().on('image', function(event, dataUrl){
				$('#img-capture-view').attr('src', dataUrl);
			});

            // console.log(preview_width)
			reset();
			initView();
			initCropper();

			// btnLive event
			$('#btnLive').on('click', function(){
				$('#capture-view').hide();
				$('#live-view').show();
			}); // end of btnLive event

			// btnCapture event
			$('#btnCapture').on('click', function(){
				$('.trigger').click();
				$('#live-view').hide();

				$('#img-capture-view').cropper('destroy');
				initCropper();

				$('#capture-view').show();
			}); // end of btnCapture event

			// btnCrop event
			$('#btnCrop').on('click', function(){
				$('#foto').attr('src', $image_captured.cropper('getCroppedCanvas', {
					width: 100,
					height: 132
				}).toDataURL())
			}); // end of btnCrop event

			// btnCoba event
			$('#btnCoba').on('click', function(){
				// console.log($('#foto').attr('src'))
				$.ajax({
					'url' : "<?=base_url('coba/addBlobData')?>",
					'data' : 'base64='+$('#foto').attr('src'),
					'type' : 'POST',
					'dataType' : 'JSON'
				})
				.done(function(response){
					console.log(response);
				})
				.fail(function(jqXHR, textStatus, errorThrown){
					console.log('Error : '+ jqXHR.responseText);
				});

			}); // end of btnCoba event

			// btnTulis event
			$('#btnTulis').on('click', function(){
				const write_nfc_url = 'https://localhost/nfc/write_card_process.php';

				$.ajax({
					'url' : write_nfc_url,
					'data' : 'npm='+$('#npm').val(),
					'type' : 'POST',
					'dataType' : 'JSON'
				})
				.done(function(response){
					console.log(response);
				})
				.fail(function(jqXHR, textStatus, errorThrown){
					console.log('Error : '+ jqXHR.responseText);
				});

			}); // end of btnTulis event

			// btnCetak event
			$('#btnCetak').on('click', function(){
				$.ajax({
					'url' : "<?=base_url('rec/generateQr')?>",
					'data' : 'npm='+$('#npm').val(),
					'type' : 'POST',
					'dataType' : 'JSON'
				})
				.done(function(response){
					// console.log(response);
					$('#foto').attr('src', getBase64Photo(response.base64_qrcode));
				})
				.fail(function(jqXHR, textStatus, errorThrown){
					console.log('Error : '+ jqXHR.responseText);
				});
			}); // end of btnCetak event
		});

		function reset() {
			$('#npm').focus();
			$('#foto').attr('src', generateDefaultPhoto());
		}

		function initView() {
			$('#capture-view').css('max-width', $('#live-view').width());
			$('#capture-view').css('max-height', $('#live-view').height());
		}

		function initCropper() {
			$image_captured.cropper({
				aspectRatio: 3 / 4,
				minCropBoxWidth: 340,
				minCropBoxHeight: 300,
				zoomable: true,
				modal: false,
				background: false,
				crop: function(e) {

				},
				ready: function () {
					$image_captured.cropper("setCropBoxData", { width: 340, height: 452 });
			    }
			});
		}
	</script>

</body>
</html>