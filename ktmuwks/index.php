<!DOCTYPE html>
<html>
<head>
	<title>Cek</title>

	<style type="text/css">

		body {
			margin: 0px;
			font-family: Calibri;
		}

		.photo-dummy {
			position: absolute;
			z-index: 1;
			opacity: 0;
		}

		.photo-mhs {
			width: 84.5px;
			height: auto;
			position: absolute;
			z-index: 2;
			left: 2.7%;
			top: 41.5%;
		}

		.identity-container {
			position: absolute;
			z-index: 2;
			left: 25%;
			top: 39.6%;
		}

		.exp-container {
			position: absolute;
			z-index: 2;
			left: 50%;
			top: 83%;
		}


	</style>
</head>
<body>
	<div class="main-container">
		<img src="ktm.PNG" class="photo-dummy">
		<div class="photo-container">
			<img src="download.png" class="photo-mhs">
		</div>
		<div class="identity-container">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td style="font-weight: bold; width: 55px;">NAMA</td>
					<td style="font-weight: bold; width: 10px;">:</td>
					<td style="font-weight: bold; width: 240px;">SRI WAHYUNI</td>
				</tr>
				<tr>
					<td style="font-weight: bold;">NPM</td>
					<td style="font-weight: bold;">:</td>
					<td style="font-weight: bold;">461304535</td>
				</tr>
				<tr>
					<td style="font-weight: bold;">Prodi</td>
					<td style="font-weight: bold;">:</td>
					<td style="font-weight: bold;">Pendidikan Dokter</td>
				</tr>
				<tr>
					<td style="font-weight: bold;">Alamat</td>
					<td style="font-weight: bold;">:</td>
					<td>Ini Alamatnya</td>
				</tr>
			</table>
		</div>
		<div class="exp-container">
			<p>Berlaku s/d Juni 2017</p>
		</div>
	</div>
</body>
</html>